using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata
{
    [TestClass]
    public class BandNameGeneratorTest
    {
        private readonly BandNameGenerator _bandNameGenerator = new BandNameGenerator();

        [TestMethod]
        public void dolphin()
        {
            _bandNameGenerator.SetBandName("dolphin");
            BandNameShouldBe("The Dolphin");
        }

        [TestMethod]
        public void sUpErMaN()
        {
            _bandNameGenerator.SetBandName("sUpErMaN");
            BandNameShouldBe("The Superman");
        }

        [TestMethod]
        public void Alaska()
        {
            _bandNameGenerator.SetBandName("alaska");
            BandNameShouldBe("Alaskaalaska");
        }

        [TestMethod]
        public void AmElIa()
        {
            _bandNameGenerator.SetBandName("AmElIa");
            BandNameShouldBe("Ameliaamelia");
        }

        [TestMethod]
        public void Null()
        {
            _bandNameGenerator.SetBandName(null);
            BandNameShouldBe("Error Input");
        }

        private void BandNameShouldBe(string expected)
        {
            Assert.AreEqual(expected, _bandNameGenerator.GetBandName());
        }
    }
}
using System.Linq;

namespace Kata
{
    public class BandNameGenerator
    {
        private string _bandName;

        public string GetBandName()
        {
            if (string.IsNullOrEmpty(_bandName))
            {
                return "Error Input";
            }

            return IsLastLetterEqualFirstLetter(_bandName, out var bandNameResult)
                ? GetCapitalCaseString(bandNameResult)
                : $"The {GetCapitalCaseString(bandNameResult)}";
        }

        private static string GetCapitalCaseString(string brandNameResult)
        {
            return $"{brandNameResult.ToUpper().First()}{brandNameResult.Substring(1).ToLower()}";
        }

        private static bool IsLastLetterEqualFirstLetter(string input, out string result)
        {
            result = input;

            if (!input.ToUpper().First().Equals(input.ToUpper().Last()))
            {
                return false;
            }

            result += input;

            return true;
        }

        public void SetBandName(string bandName)
        {
            _bandName = bandName;
        }
    }
}
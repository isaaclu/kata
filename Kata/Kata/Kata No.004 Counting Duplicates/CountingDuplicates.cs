﻿using System.Linq;

namespace Kata
{
    public class CountingDuplicates
    {
        private string _input;

        public void SetString(string input)
        {
            _input = input;
        }

        public int GetDuplicateCount()
        {
            if (string.IsNullOrEmpty(_input))
            {
                return 0;
            }

            var repeatChar = _input.ToLower().ToCharArray()
                .GroupBy(x => x).Where(y => y.Count() > 1)
                .Select(z => z.Key);

            return repeatChar.Count();
        }
    }
}
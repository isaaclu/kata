﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata
{
    [TestClass()]
    public class CountingDuplicatesTest
    {
        private CountingDuplicates _countingDuplicates = new CountingDuplicates();

        [TestMethod]
        public void EmptyString()
        {
            _countingDuplicates.SetString("");
            ShouldBeEqual(0);
        }

        [TestMethod]
        public void abcde()
        {
            _countingDuplicates.SetString("abcde");
            ShouldBeEqual(0);
        }

        [TestMethod]
        public void aabbccde()
        {
            _countingDuplicates.SetString("aabbcde");
            ShouldBeEqual(2);
        }

        [TestMethod]
        public void aabBcde()
        {
            _countingDuplicates.SetString("aabBcde");
            ShouldBeEqual(2);
        }

        [TestMethod]
        public void indivisibility()
        {
            _countingDuplicates.SetString("indivisibility");
            ShouldBeEqual(1);
        }

        [TestMethod]
        public void Indivisibilities()
        {
            _countingDuplicates.SetString("Indivisibilities");
            ShouldBeEqual(2);
        }

        [TestMethod]
        public void aA11()
        {
            _countingDuplicates.SetString("aA11");
            ShouldBeEqual(2);
        }

        [TestMethod]
        public void ABBA()
        {
            _countingDuplicates.SetString("ABBA");
            ShouldBeEqual(2);
        }

        private void ShouldBeEqual(int expected)
        {
            Assert.AreEqual(expected, _countingDuplicates.GetDuplicateCount());
        }
    }
}
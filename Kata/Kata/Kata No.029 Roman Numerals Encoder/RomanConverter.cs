﻿using System.Collections.Generic;

namespace Kata
{
    internal class RomanConverter
    {
        private readonly Dictionary<int, string> _dicRomanNumber = new Dictionary<int, string>()
        {
            {1,"I"},
            {5,"V"},
            {10,"X"},
        };

        private int _input;

        public string GetSymbol()
        {
            return _dicRomanNumber[_input];
        }

        public void SetInput(int input)
        {
            _input = input;
        }
    }
}
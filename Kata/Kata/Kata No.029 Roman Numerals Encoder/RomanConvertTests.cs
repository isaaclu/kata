﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata
{
    [TestClass()]
    public class RomanConvertTests
    {
        private readonly RomanConverter _romanConverter = new RomanConverter();

        [TestMethod]
        public void One_should_equal_I()
        {
            SetNumber(1);
            ShouldBeEqual("I");
        }

        [TestMethod]
        public void Five_should_equal_V()
        {
            SetNumber(5);
            ShouldBeEqual("V");
        }

        [TestMethod]
        public void Ten_should_equal_X()
        {
            SetNumber(10);
            ShouldBeEqual("X");
        }

        private void SetNumber(int input)
        {
            _romanConverter.SetInput(input);
        }

        private void ShouldBeEqual(string expected)
        {
            Assert.AreEqual(expected, _romanConverter.GetSymbol());
        }
    }
}
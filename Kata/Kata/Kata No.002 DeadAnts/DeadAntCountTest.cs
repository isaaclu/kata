﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata
{
    [TestClass()]
    public class DeadAntCountTest
    {
        private AntCount _antCount = new AntCount();

        [TestMethod()]
        public void ZeroDeadAntCount()
        {
            SetAntString("..ant..ant.ant...ant.ant..ant.ant....ant..ant.ant.ant...ant..");
            ShouldBeEqual(0);
        }

        [TestMethod()]
        public void OneDeadAntCount()
        {
            SetAntString("..an..ant.ant...ant.ant..ant.ant....ant..ant.ant.ant...ant..");
            ShouldBeEqual(1);
        }

        [TestMethod()]
        public void NullDeadAnt()
        {
            SetAntString(null);
            ShouldBeEqual(0);
        }

        [TestMethod()]
        public void TwoDeadAntCount()
        {
            SetAntString("ant anantt aantnt");
            ShouldBeEqual(2);
        }

        [TestMethod()]
        public void ThreeDeadAntCount()
        {
            SetAntString("ant anantt aantnta");
            ShouldBeEqual(3);
        }

        [TestMethod()]
        public void ThreeDeadAntCountWithOnlyA()
        {
            SetAntString("aaa");
            ShouldBeEqual(3);
        }

        [TestMethod()]
        public void FourDeadAntCountWithOnlyAInCapitalCase()
        {
            SetAntString("aaAa");
            ShouldBeEqual(4);
        }

        [TestMethod()]
        public void FiveDeadAntCountWithAtMostFiveDeadPart()
        {
            SetAntString("aaaannnnntt");
            ShouldBeEqual(5);
        }

        private void ShouldBeEqual(int expected)
        {
            Assert.AreEqual(expected, _antCount.GetDeadAntCount());
        }

        private void SetAntString(string input)
        {
            _antCount.SetAntString(input);
        }
    }
}
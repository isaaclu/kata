﻿using System.Linq;

namespace Kata
{
    public class AntCount
    {
        private string _input;

        public int GetDeadAntCount()
        {
            return string.IsNullOrEmpty(_input) ? 0 : CountDeadAnt(RemoveAliveAnt());
        }

        private static int CountDeadAnt(string replaceInput)
        {
            return "ant".Select(charAnt => replaceInput.Count(x => x.Equals(charAnt))).Concat(new[] { 0 }).Max();
        }

        private string RemoveAliveAnt()
        {
            return _input.ToLower().Replace("ant", "").Replace(".", "").Replace(" ", "");
        }

        public void SetAntString(string input)
        {
            _input = input;
        }
    }
}
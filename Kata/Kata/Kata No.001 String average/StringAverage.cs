﻿using System.Collections.Generic;
using System.Linq;

namespace Kata
{
    public class StringAverage
    {
        private string _input;

        private readonly Dictionary<string, int> _dicNumber = new Dictionary<string, int>()
        {
            { "zero", 0},
            { "one", 1},
            { "two", 2},
            { "three", 3},
            { "four", 4},
            { "five", 5},
            { "six", 6},
            { "seven", 7},
            { "eight", 8},
            { "nine", 9},
        };

        public string GetStringAverage()
        {
            if (string.IsNullOrEmpty(_input))
            {
                return "n/a";
            }

            if (TryParseStringToInt(_input, out var transferredNumber)
                && _dicNumber.ContainsValue((int)transferredNumber.Average()))
            {
                return GetStringByNumber((int)transferredNumber.Average());
            }

            return "n/a";
        }

        private string GetStringByNumber(int input)
        {
            return _dicNumber.FirstOrDefault(x => x.Value.Equals(input)).Key;
        }

        private bool TryParseStringToInt(string input, out List<int> transferredNumber)
        {
            transferredNumber = new List<int>();

            foreach (var numberString in input.Split(" "))
            {
                if (!_dicNumber.ContainsKey(numberString.ToLower()))
                {
                    return false;
                }
                transferredNumber.Add(_dicNumber[numberString.ToLower()]);
            }

            return true;
        }

        public void SetString(string input)
        {
            _input = input;
        }
    }
}
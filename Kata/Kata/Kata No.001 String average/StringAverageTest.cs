﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata
{
    [TestClass()]
    public class StringAverageTest
    {
        private readonly StringAverage _stringAverage = new StringAverage();

        [TestMethod]
        public void Zero_Nine_Five_Two()
        {
            SetString("zero nine five two");
            ShouldBeEqual("four");
        }

        [TestMethod]
        public void Six_Nine_Five_Two()
        {
            SetString("six nine five two");
            ShouldBeEqual("five");
        }

        [TestMethod]
        public void SiX_nInE_fIvE_tWo()
        {
            SetString("SiX nInE fIvE tWo");
            ShouldBeEqual("five");
        }

        [TestMethod]
        public void ssSiX_nInE_fIvE_tWo()
        {
            SetString("ssSiX nInE fIvE tWo");
            ShouldBeEqual("n/a");
        }

        [TestMethod]
        public void SiXteen_nInE_fIvE_tWo()
        {
            SetString("SiXteen nInE fIvE tWo");
            ShouldBeEqual("n/a");
        }

        [TestMethod]
        public void EmptyString()
        {
            SetString("");
            ShouldBeEqual("n/a");
        }

        [TestMethod]
        public void NullString()
        {
            SetString(null);
            ShouldBeEqual("n/a");
        }

        [TestMethod]
        public void SpecialCharacter()
        {
            SetString("SiX nInE %$#%# tWo");
            ShouldBeEqual("n/a");
        }

        private void ShouldBeEqual(string expected)
        {
            Assert.AreEqual(expected, _stringAverage.GetStringAverage());
        }

        private void SetString(string input)
        {
            _stringAverage.SetString(input);
        }
    }
}
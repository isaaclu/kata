﻿namespace Kata
{
    public class MiddleCharacter
    {
        private string _input;

        public void SetString(string input)
        {
            _input = input;
        }

        public string GetMiddleCharacter()
        {
            if (!string.IsNullOrEmpty(_input))
            {
                return (_input.Length % 2).Equals(0)
                    ? _input.Substring(_input.Length / 2 - 1, 2)
                    : _input.Substring(_input.Length / 2, 1);
            }

            return "";
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata
{
    [TestClass()]
    public class GetMiddleTest
    {
        private MiddleCharacter _middleCharacter = new MiddleCharacter();

        [TestMethod()]
        public void GetMiddle_test()
        {
            _middleCharacter.SetString("test");
            ShouldBeEqual("es");
        }

        [TestMethod()]
        public void GetMiddle_testing()
        {
            _middleCharacter.SetString("testing");
            ShouldBeEqual("t");
        }

        [TestMethod()]
        public void GetMiddle_middle()
        {
            _middleCharacter.SetString("middle");
            ShouldBeEqual("dd");
        }

        [TestMethod()]
        public void GetMiddle_A()
        {
            _middleCharacter.SetString("A");
            ShouldBeEqual("A");
        }
        private void ShouldBeEqual(string expected)
        {
            Assert.AreEqual(expected, _middleCharacter.GetMiddleCharacter());
        }
    }
}